# ITC Seismology 2023 Montenegro - Moment Tensor Inversion

## Lecturers

* Simone Cesca <cesca@gfz-potsdam.de>
* Gesa Petersen <gesap@gfz-potsdam.de>
* Sebastian Heimann <sebastian.heimann@uni-potsdam.de>

## Chat

Please join the [itc2023
channel](https://hive.pyrocko.org/pyrocko-support/channels/itc2023) on the
[Pyrocko Hive](https://hive.pyrocko.org).

A [command log](https://hive.pyrocko.org/pyrocko-support/channels/itc2023-log)
is also available there, where you can copy the shell commands which are shown
during the lectures.

## Program

**Thursday, October 19**

* 08:30 - 10:00: Moment Tensor Inversion - Theory
* 10:30 - 12:00: Earthquake Data, Agencies and Formats
* 13:30 - 15:00: Data Access, Preparation and Visualization
* 15:30 - 17:00: Green’s Functions

**Friday, October 20**

* 08:30 - 10:00: Synthetic Seismograms
* 10:30 - 12:00: Moment Tensor Inversion with Grond
* 13:30 - 15:00: Moment Tensor Inversion Exercise I
* 15:30 - 17:00: Moment Tensor Inversion Exercise II

## Software

Some special software is needed to perform our exercises.

**Option 1: Use our preconfigured virtual machine**

Download the following VM image and import it into [VirtualBox](https://www.virtualbox.org/wiki/Downloads).

* [itc2023-r3.ova](https://geofon.gfz-potsdam.de/training/itc2023-r3.ova) [info](https://geofon.gfz-potsdam.de/training/) - VM image with all software pre-installed

Choose *Import Appliance* from the *File* menu and select the downloaded VM
image. Then start the VM. The password is `earthquake101` but it is configured
that you don't have to enter it.

In the guest system, open a terminal and type

```
curl -s https://data.pyrocko.org/scratch/itc-update | bash -s
```

to get all software up to date.

Then run

```
curl -s https://data.pyrocko.org/scratch/itc-update-mt-dataset | bash -s
```

to get the exercise dataset (2.2 GB).


**Option 2: Use your own computer**

Install the following software on your Linux or Mac machine (Windows may
also work but is not recommended):

* [Pyrocko](https://pyrocko.org/docs/current/install/index.html)
* [Grond](https://pyrocko.org/grond/docs/current/install/index.html)
* [QSEIS Backend for Fomosto](https://git.pyrocko.org/pyrocko/fomosto-qseis)

And get the training dataset [itc-seis-dataset-2023-montenegro.tar](https://data.pyrocko.org/scratch/itc-seis-dataset-2023-montenegro.tar)

## Presentations

* [Source inversion in seismology (Moment Tensor Inversion - Theory)](https://data.pyrocko.org/itc/2023/presentations/cesca_itc2023potsdam_sourceinversion.pdf)
* [The World of Seismological Data (Earthquake Data, Agencies and Formats)](https://data.pyrocko.org/itc/2023/presentations/lecture-data-new/)
* [Pyrocko Squirrel (Data Access, Preparation and Visualization)](https://data.pyrocko.org/itc/2023/presentations/squirrel-2022/)
* [Pyrocko Snuffler (Data Access, Preparation and Visualization)](https://data.pyrocko.org/presentations/pyrocko-workshop-dgg-2021/#/snuffler)
* [Pyrocko GF (Green's Functions)](https://data.pyrocko.org/itc/2023/presentations/presentation-gf.pdf)
* [Grond - The practical side (Moment Tensor Inversion with Grond)](https://data.pyrocko.org/itc/2023/presentations/grond-intro)

## Links and Tutorials

* [Pyrocko web page](https://pyrocko.org)
* [Pyrocko manual](https://pyrocko.org/docs/current/)
* [Snuffler manual (Data Access, Preparation and Visualization)](https://pyrocko.org/docs/current/apps/snuffler/index.html)
* [Squirrel command line tool tutorial (Data Access, Preparation and Visualization)](https://pyrocko.org/docs/current/apps/squirrel/tutorial.html)
* [Pyrocko GF overview (Green's Functions)](https://pyrocko.org/docs/current/topics/pyrocko-gf.html)
* [Fomosto Tutorial (Green's Functions, Synthetic Seismograms)](https://pyrocko.org/docs/current/apps/fomosto/tutorial.html)
* [Grond manual (Moment Tensor Inversion with Grond)](https://pyrocko.org/grond)


## Hints for running your own inversion:

### 1. Catalog search
* Find an event (M 5-6) in the [Geofon catalog](https://geofon.gfz-potsdam.de/eqinfo/list.php). Please try to use an event in which you expect a good station coverage with stations that are accessable via fdsn (e.g., iris, geofon, ingv, orfeus, koeri, ...).

* Examples: [gfz2022ttgm](http://geofon.gfz-potsdam.de/eqinfo/event.php?id=gfz2022ttgm), [gfz2023shhb](http://geofon.gfz-potsdam.de/eqinfo/event.php?id=gfz2023shhb), [gfz2021zhje](http://geofon.gfz-potsdam.de/eqinfo/event.php?id=gfz2021zhje)


### 2. Prepare grond

* ```cd ~```: Go to your /home/ directory (or to another place where you want to run the new inversion).

* ```grond init example_regional_cmt my_first_grond_project```

* ```cd my_first_grond_project```

* ```mkdir gf_stores```

* ```cd gf_stores```


### 3. Download Green's function database for forward modeling
* Find out which crust2 velocity model is suitable for your location. Type `python3` in the terminal to open a python console:
```
from pyrocko import crust2x2

print(crust2x2.get_profile(lat=event_latitude, lon=event_longitude))

>>> type, name: H1, early/mid Proter., no seds.
>>> ...

```
Close the python console afterward by typing `exit()`.


* Download the gf_store: ```fomosto download kinherd crust2_2hz_xx``` - replace the `xx` with the handle for the wanted store.

### 4. Get the waveform data and metadata:
* ```cd ..``` to get back from the `gf_stores` directory to the `my_first_grond_project` directory.

* If you want to download data from other providers than the default (geofon,iris,orfeus) open `bin/grondown_regional.sh` and add `--sites='ingv,orfeus,geofon'`. For example to invert for an Italian earthquake, you could work with stations from 'ingv'... You can also adjust for example the maximum distance, the default is 1000 km.

* run grondown: ```bin/grondown_regional.sh <gfz-eventname>```

* Make sure that you have at least about 10-20 stations in 50 to 500 km to the event. Look at the waveforms with the snuffer and perform a thorough quality check.

### 5. Grond inversion
* Check the grond config file and make any adjustments that are needed (e.g., station filename; green's function database name) or that you want to test (e.g., different frequency band; removing stations with bad signal-to-noise ratio; ...).
* First run grond check, if it is successful, run grond go as shown before...



