# Short introduction to the command line/ terminal

## Autocomplete

Hit `TAB ↹` to autocomplete any path. If more than one option is available, hit `TAB ↹` twice to display all possible choices.


## Command line history

Use the arrow up and down keys (`↑↓`) to navigate in the command line to commands that you have used before.


## Wildcard in paths:

Use `*` as a wildcard in paths, e.g., `snuffler data/events/*/waveforms/raw` to display all trances in the `waveforms/raw` directories of all event directories.


## Where am I? How do I navigate to directories?

`pwd`: ***p**rint **w**orking **d**irectory*. Shows the path of the current directory.

`cd`: ***c**hange **d**irectory*. To navigate to another directory. Without any argument, it will navigate to the home directory.

`cd ..`: Navigate one directory upwards.

`cd ../..`: Navigate two directories upwards.

`cd <directory name> or <path>`: Navigate to a specific directory or path.



## Display the content of a directory


`ls`: List all files and subdirectories. Without options and argument, `ls` displays the content of your current working directory in alphabetic order.

`ls -l`: Option `-l` shows more information, e.g. the time of last changes and size.

`ls <path>`: Adding a path as argument to `ls` displays the content of the directory at the path.


## Copy a directory or file

`cp <source> <destination>`: Copy a file from source to destination. Add option `-r` to copy **r**ecursively all content of `<source>` including its' subdirectories to a `<destination>`


## Make a new directory

`mkdir <my_directory>`: Make a new directory called `<my_directory>` at the current location.


## Open a text editor from the terminal

`gedit`: Type `gedit` to open the gedit editor in the VM. By adding `&` in the end of the command (`gedit &`), you can continue working in the same terminal.



## Commands used to update and download software and data for our exercises

`curl -s https://data.pyrocko.org/scratch/itc-update | bash -s`

`curl -s https://data.pyrocko.org/scratch/itc-update-mt-dataset | bash -s`


`curl`: * **C**lient for **URL**s* is used to transfer/download data. `-s` mutes curl to avoid progress reports for the downloads.

`|`: The pipe (`|`) is used to run several commands sequentially. In the two commands above first `curl` is used to access the update and download scripts itc-update and itc-update-mt-dataset. Then `bash` is called: 

`bash -s`: `bash` is called to execute a BASH code, here the itc-update and itc-update-mt-dataset. The `-s` option says that commands are read from standard input, in our case this is the content of the scripts.



## Execute bash scripts

`chmod a+x <file>`: `chmod` (short for **ch**ange **mod**e) is used to change the access permissions of files and directories. We use it to change permissions of scripts that we want to make executable in the shell. `a+x` indicates that **a**ll users shall be able to e**x**ecute the file.

